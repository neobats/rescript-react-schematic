type fileExtension = 
| None
| JS
| JSX
| TS
| TSX
| Res
| Unknown(string)

let parseExtension = ext => {
  switch Js.String.toLowerCase(ext) {
    | "js"
    | "jsx" => JS
    | "ts"
    | "tsx" => TS
    | "res" => Res
    | _ => Unknown(ext)
  }
}

let makeExtensionText = ext => switch ext {
| TS 
| TSX => ": React.FC<{}>"
| Res
| JS
| JSX
| _ => "" 
}

type styleOptions = {
  semi: bool,
  singleQuote: bool,
  emotion: bool
}

let generateComponentText = (~name: string, ~props: bool, ~fileType: fileExtension, ~options: styleOptions) => {
  open StringHelpers
  let title = capitalize(name) ++ " Works!"
  let propsText = if props {
    "props"
  } else {
    ""
  }
  let (emotionImport, cssText) = if options.emotion {
    ("import { css, jsx } from '@emotion/react",
    ` css={
      padding: .2em
    }`)
  } else {
    ("", "")
  }
  let extraText = makeExtensionText(fileType)

  `
  import React from 'react'
  ${emotionImport}

  export default function ${name}(${propsText})${extraText} {
    return (
      <>
        <h1${cssText}>${title}</h1>
      </>
    )
  }
  `
}

let validate = entry => StringHelpers.isNotEmpty(entry) ? entry : ""

type componentFile = {
  name: string,
  extension: fileExtension
}

type unconvertedFile = {name: string, ext: string}

let convertFile = ({name, ext}: unconvertedFile) => {
  open StringHelpers
  switch ({name, ext}) {
    | {name, ext} when isNotEmpty(name) && isNotEmpty(ext) => {name, extension: parseExtension(ext)}
    | {name, _} when isNotEmpty(name) => {name, extension: parseExtension("js")} // TODO: change to config driven or based on last used extension
    | _ => {name: "", extension: parseExtension("")}
  }
}

let convertExtension = ({name, ext}): componentFile => {name, extension: parseExtension(ext)}

let interpolateFileName = (fileName: string) => {
  open StringHelpers
  let index = Js.String.lastIndexOf(".", fileName)
  switch splitIndex(fileName, index) {
    | Some({first, second}) => {name: validate(first), ext: validate(second)}
    | None => {name: "", ext: ""}
  }
}

let readAndGenerate = (componentGenerator, fileName: string) => {
  let {name, _} = fileName -> interpolateFileName -> convertFile
  
  props => componentGenerator(~name, ~props)
}

// fileName: string, props: bool => componentText
let makeComponentText = readAndGenerate(generateComponentText)
