let capitalize = (str: string) => Js.String.toLocaleUpperCase(Js.String.get(str, 0)) ++ Js.String.sliceToEnd(~from=1, str)

let isNotEmpty = (str: string) => Js.String.length(str) > 0

type splitString = {first: string, second: string}
let splitIndex = (str: string, index: int) => {
  if isNotEmpty(str) && index > -1 && index < Js.String.length(str) {
    Some({
      first: Js.String.substring(~from=0, ~to_=index, str),
      second: Js.String.substringToEnd(~from=index, str)
    })
  } else {
    None
  }
}
