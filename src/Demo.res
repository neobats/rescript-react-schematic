@bs.module("fs") external readFileSync : string => string => Buffer.t = "readFileSync"

// writes out to Buffer...
readFileSync("../example.txt", "utf8") -> Js.log
